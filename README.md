# OCT QA

This gear assesses the quality of OCT volumes and their slices.
Three indexes are calculated: 
1) Mean and variance of the black region of the retinal image.
2) The slope of the image intensity measuring from the black region into the retina.
3) Dynamic range of the retinal image.

The gear will:
- Update the metadata of the input file with these three indexes.
- Generate histograms for three indexes so that low-quality slices can be flagged.
- Output csv with slice QA indexes for further analysis.

## Usage

### Inputs

* __raw_input__: Raw OCT slices (.npy, .dcm or .dcm.zip)

### Configuration
* __debug__ (boolean, default False): Include debug statements in output.


## Contributing

For more information about how to get started contributing to that gear, 
checkout [CONTRIBUTING.md](CONTRIBUTING.md).
