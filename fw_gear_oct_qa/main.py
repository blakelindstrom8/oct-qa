"""Main module."""

import logging
import sys

from flywheel_gear_toolkit import GearToolkitContext

from .oct_qa import OCTqa

log = logging.getLogger(__name__)


def run(ophtha_qa: OCTqa):
    """
    Calculates three indexes of OCT quality assessment.
    OCT Quality Assessment. Three indexes are calculated:
        1) mean and variance of the black region of the retinal image
        2) the slope of the image intensity measuring from the black region into the retina
        3) dynamic range of the retinal image.

    Args:
    ---------
    ophtha_qa: OCT-qa
        A concrete instance of OCT-qa.

    """
    log.info("Starting OCT-qa gear...")
    log.info("Output directory: %s", ophtha_qa.output_dir)
    try:
        # Call the template method for aligning OCT slices
        QA_index = ophtha_qa.assess_quality()
    except Exception:
        log.exception("OCT quality assessment failed")
        log.info("Exiting...")
        sys.exit(1)
    else:
        log.info("Done with OCT-qa gear.")
        return 0, QA_index
