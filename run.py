#!/usr/bin/env python
"""The run script"""
import logging
import sys

import numpy as np
from flywheel_gear_toolkit import GearToolkitContext

# This design with a separate main and parser module
# allows the gear to be publishable and the main interfaces
# to it can then be imported in another project which enables
# chaining multiple gears together.
from fw_gear_oct_qa.main import run
from fw_gear_oct_qa.parser import parse_config

# The run.py should be as minimal as possible.
# The gear is split up into 2 main components. The run.py file which is executed
# when the container runs. The run.py file then imports the rest of the gear as a
# module.


log = logging.getLogger(__name__)


def main(gear_context: GearToolkitContext) -> None:  # pragma: no cover
    """Parses config and run"""

    # Call the fw_gear_oct_qa.parser.parse_config function
    # to extract the args, kwargs from the context (e.g. config.json).
    ophtha_qa = parse_config(gear_context=gear_context)
    # Pass the args, kwargs to fw_gear_oct_qa.main.run function to execute
    # the main functionality of the gear.
    e_code, QA_index = run(ophtha_qa)
    log.info("Updating metadata...")
    update_qa_metadata(gear_context, QA_index)
    # Exit the python script (and thus the container) with the exit
    # code returned by example_gear.main.run function.
    sys.exit(e_code)


def update_qa_metadata(
    gear_context,
    QA_index,
):
    """
    A template method that updates the metadata of the OCT slices with the quality index.
    Args:
        gear_context: GearToolkitContext object.
        QA_index:
    """

    # Update the input file (e.g. acquisition file) custom info
    if not QA_index["Outliers Dynamic range"]:
        QA_index["Outliers Dynamic range"] = "No slices flagged"
    if not QA_index["Outliers Variance Extraretinal"]:
        QA_index["Outliers Variance Extraretinal"] = "No slices flagged"
    if not QA_index["Outliers Variance Intraretinal"]:
        QA_index["Outliers Variance Intraretinal"] = "No slices flagged"
    if not QA_index["Outliers Sharpness slope"]:
        QA_index["Outliers Sharpness slope"] = "No slices flagged"

    updates = {
        "Custom": {
            "Mean Dynamic range": round(np.mean(QA_index["Dynamic range"]), 3),
            "Standard deviation Dynamic range": round(
                np.std(QA_index["Dynamic range"]), 3
            ),
            "Outliers Dynamic range": QA_index["Outliers Dynamic range"],
            "Mean Variance Extraretinal region": round(
                np.mean(QA_index["Variance extraretinal"]), 3
            ),
            "Standard deviation Variance Extraretinal region": round(
                np.std(QA_index["Variance extraretinal"]), 3
            ),
            "Outliers Variance Extraretinal": QA_index[
                "Outliers Variance Extraretinal"
            ],
            "Mean Variance Intraretinal": round(
                np.mean(QA_index["Variance intraretinal"]), 3
            ),
            "Standard deviation Variance Intraretinal": round(
                np.std(QA_index["Variance intraretinal"]), 3
            ),
            "Outliers Variance Intraretinal": QA_index[
                "Outliers Variance Intraretinal"
            ],
            "Mean Sharpness slope": round(np.mean(QA_index["Sharpness slope"]), 3),
            "Standard deviation Sharpness slope": round(
                np.std(QA_index["Sharpness slope"]), 3
            ),
            "Outliers Sharpness slope": QA_index["Outliers Sharpness slope"],
        }
    }

    raw_input = gear_context.get_input_filename("raw_input")
    gear_context.update_file_metadata(raw_input, info=updates)


# Only execute if file is run as main, not when imported by another module
if __name__ == "__main__":  # pragma: no cover
    # Get access to gear config, inputs, and sdk client if enabled.
    with GearToolkitContext() as gear_context:

        # Initialize logging, set logging level based on `debug` configuration
        # key in gear config.
        gear_context.init_logging()

        # Pass the gear context into main function defined above.
        main(gear_context)
